import 'package:flutter/material.dart';

abstract class GetProductStates {}
class AuthInitializedStates extends GetProductStates {}

class GetDataProductStatesLoading extends GetProductStates{}
class GetDataProductStatesSuccess extends GetProductStates{}
class GetDataProductErrorStates extends GetProductStates{
  final String error;
  GetDataProductErrorStates(this.error);
}

//
//
// class GetDataDevicesHistoryLoding extends GetCategourStates{}
// class GetDataDevicesHistoryStatesSuccess extends GetCategourStates{}
// class GetDataDevicesHistoryErrorStates extends GetCategourStates{
//   final String error;
//   GetDataDevicesHistoryErrorStates(this.error);
// }
