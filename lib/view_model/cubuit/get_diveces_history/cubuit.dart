

import 'package:bloc/bloc.dart';
import 'package:clinico/view_model/cubuit/get_diveces_history/states.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../model/bought_devices.dart';

/////////// this GetDeviceHistory
class GetDeviceHistory extends Cubit<GetDevicesHistoryStates> {
  GetDeviceHistory() : super(AuthInitializedStates());

  static GetDeviceHistory get(context) =>
      BlocProvider.of<GetDeviceHistory>(context);

  List<DevicesHistory> divesHistory = [];

  Future<void> getDivesHistory() async {
    emit(GetDataDevicesHistoryLoding());
    await FirebaseFirestore.instance
        .collection("DevicesHistory")
        .get()
        .then((value) {
      value.docs.forEach((element) {
          divesHistory.add(DevicesHistory.fromJson(element.data()));
      });
    }).catchError((error) {
      emit(GetDataDevicesHistoryErrorStates(error.toString()));
    });
  }
// get data form cat
}