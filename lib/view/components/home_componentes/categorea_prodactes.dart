import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants/app_colors.dart';
import '../../../model/home_card.dart';
import '../../../view_model/cubuit/get_categore_from_firebase_cubuit/cubuit.dart';
import '../../../view_model/cubuit/get_categore_from_firebase_cubuit/states.dart';
import '../../screens/category_screen/category_list_screen.dart';
import '../../screens/device_list_screen.dart';
import '../custom_text/custom_text.dart';

class CategorisProdactes extends StatelessWidget {
  const CategorisProdactes({
    Key? key,
    required this.homeCards,
  }) : super(key: key);

  final List<HomeCard> homeCards;

  @override
  Widget build(BuildContext context) {
    var getDataCategour = BlocProvider.of<GetCategersCubit>(
        context, listen: true);

    return BlocConsumer<GetCategersCubit, GetCategourStates>(
      listener: (context, state) {
        // TODO: implement listener
      },
      builder: (context, state) {
        return ConditionalBuilder(
          condition: getDataCategour.Categorus.length > 0,
          builder: (BuildContext context) =>
              Center(
                child: Container(
                  padding: EdgeInsets.only(right: 23, left: 10),
                  height: 160,
                  child: ListView.separated(
                    itemCount: getDataCategour.Categorus.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) {
                                return CategoryListScreen(
                                    getDataCategour.Categorus[index],
                                );
                              }));
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 15),
                          height: 130,
                          width: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: Center(
                            child: Column(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(40.0),
                                  child: Image.network(
                                    getDataCategour.Categorus[index].image
                                        .toString(),
                                    fit: BoxFit.fill,
                                    width: 90,
                                    height: 90,
                                  ),
                                ),
                                SizedBox(height: 15),
                                Container(
                                  width: 60,
                                  child: CustomText(
                                    textAlign: TextAlign.center,
                                    text: getDataCategour.Categorus[index]
                                        .arabicTitle.toString(),
                                    fontSize: 15,
                                    maxLines: 2,
                                    color: textColorblack,
                                    fontWeight: FontWeight.w400,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        SizedBox(
                          width: 15,
                        ),
                  ),
                ),
              ),
          fallback: (context) =>  Center(
            child: CircularProgressIndicator()
          ),
        );
      },
    );
  }
}
