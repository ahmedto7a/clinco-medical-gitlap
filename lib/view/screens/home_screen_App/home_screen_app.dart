import 'package:clinico/model/user_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../constants/app_colors.dart';
import '../../../helper/shared_preferences.dart';
import '../../../model/ad.dart';
import '../../../model/devices_category.dart';
import '../../../model/home_card.dart';
import '../../../model/speciality.dart';
import '../../../view_model/cubuit/get_producat/cubuit.dart';
import '../../../view_model/cubuit/get_producat/states.dart';
import '../../components/custom_text/custom_text.dart';
import '../../components/home_componentes/ads_container_widget.dart';
import '../../components/home_componentes/categorea_prodactes.dart';
import '../../components/home_componentes/categores_card.dart';
import '../speciality_data_screen.dart';
import 'devices_categories_fragment.dart';

class HomeScreenApp extends StatefulWidget {
  const HomeScreenApp({Key? key}) : super(key: key);

  @override
  _HomeScreenAppState createState() => _HomeScreenAppState();
}

//
class _HomeScreenAppState extends State<HomeScreenApp> {
  Color primaryLightColor = AppColors.primaryColorold;
  List<Color> primaryGradientColors = AppColors.primaryGradientColors;
  String? _userAccountDocumentReferencePath, _userAccountDocumentReferenceId;

  List<Speciality> specialties = <Speciality>[];
  late CollectionReference specialtiesRef;
  late final AppData _appData;
  String? _userId, _selectedCountry, _accountType;
  bool _isAdmin = false;
  List<Ad> ads = <Ad>[];
  FirebaseFirestore? _firebaseFirestore;
  late Query<Map<String, dynamic>>? queryClinicsAdsRef;
  CollectionReference<Map<String, dynamic>>? _accountCollectionReference;
  Speciality? _doctorSpeciality;

  @override
  void initState() {
    _firebaseFirestore = FirebaseFirestore.instance;
    _appData = AppData();
    _appData.getSharedPreferencesInstance().then((pref) {
      setState(() {
        _userId = _appData.getUserId(pref!)!;
        _selectedCountry = _appData.getSelectedCountry(pref)!;
        _accountType = _appData.getAccountType(pref);
        _isAdmin = (_accountType == AccountTypes.Admin.name);
        queryClinicsAdsRef = _firebaseFirestore!
            .collection(FirestoreCollections.ClinicsAds.name)
            .where('selectedCountry', isEqualTo: _selectedCountry)
            .where('isActive', isEqualTo: true)
            .where('expiryDate', isGreaterThanOrEqualTo: DateTime.now())
            .orderBy('expiryDate', descending: false)
            .orderBy('priority', descending: false);
        getClinicsAds();
      });
      getAccountData();
    });
    specialtiesRef = FirebaseFirestore.instance
        .collection(FirestoreCollections.Specialties.name);
    super.initState();
  }

  Future<void> getAccountData() async {
    AccountTypes type = AccountTypes.values.byName(_accountType.toString());
    switch (type) {
      case AccountTypes.Admin:
        _accountCollectionReference =
            _firebaseFirestore!.collection(FirestoreCollections.Admins.name);
        break;
      case AccountTypes.Doctor:
        _accountCollectionReference =
            _firebaseFirestore!.collection(FirestoreCollections.Doctors.name);
        break;
      case AccountTypes.Company:
        _accountCollectionReference =
            _firebaseFirestore!.collection(FirestoreCollections.Companies.name);
        break;
      default:
        _accountCollectionReference =
            _firebaseFirestore!.collection(FirestoreCollections.Patients.name);
    }
    _accountCollectionReference
        ?.where('userId', isEqualTo: _userId)
        .get()
        .then((snapshots) {
      QueryDocumentSnapshot<Map<String, dynamic>> queryDocumentSnapshot =
          snapshots.docs.first;
      if (queryDocumentSnapshot.exists) {
        setState(() {
          _userAccountDocumentReferencePath =
              queryDocumentSnapshot.reference.path;
          _userAccountDocumentReferenceId = queryDocumentSnapshot.reference.id;
          userAccountJsonMap = queryDocumentSnapshot.data();
        });
        if (userAccountJsonMap?['specialityId'] != null &&
            userAccountJsonMap!['specialityId'].toString().trim().isNotEmpty) {
          getDoctorSpeciality();
        }
      }
    });
  }

  void getDoctorSpeciality() {
    _firebaseFirestore
        ?.collection(FirestoreCollections.Specialties.name)
        .doc(userAccountJsonMap!['specialityId'])
        .get()
        .then((value) {
      setState(() {
        _doctorSpeciality = Speciality.fromJson(value.data());
      });
    });
  }

  // demo list for card in home screen
  List<HomeCard> homeCards = [
    HomeCard(
        pageName: "مكالمة دكتور", pageImage: "assets/images/call_doctor.png"),
    HomeCard(pageName: "حجز عيادة", pageImage: "assets/images/doctor_lo.png"),
    // HomeCard(pageName: "مراكز الاشعة", pageImage: "assets/images/rays.png"),
    // HomeCard(pageName: "معامل التحاليل", pageImage: "assets/images/analisis.png"),
    HomeCard(
        pageName: "الأجهزة الطبية", pageImage: "assets/images/devices.png"),
    HomeCard(
        pageName: "مؤشر كتلة الجسم", pageImage: "assets/images/weight.png"),
    HomeCard(pageName: "العيادات", pageImage: "assets/images/clinic.png"),
    HomeCard(
        pageName: "خريطة العيادات",
        pageImage: "assets/images/clinic_location.png"),
  ];

  getClinicsAds() async {
    await queryClinicsAdsRef!.get().then((value) => {
          setState(() {
            ads.clear();
            value.docs
                .asMap()
                .forEach((key, json) => ads.add(Ad.fromJson(json)));
          })
        });
  }

  Map<String, dynamic>? userAccountJsonMap;
  List<DevicesCategory> devicesCategories = <DevicesCategory>[];

  @override
  Widget build(BuildContext context) {
    var getDataCategour = BlocProvider.of<GetProductCubit>(
        context, listen: true);
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          backgroundColor: Colors.grey[100],
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          floatingActionButton: Visibility(
            visible: _isAdmin,
            child: FloatingActionButton(
              onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => SpecialityDataScreen(
                      isNewItem: true,
                      specialityDocumentPath: null,
                      speciality: null))),
              backgroundColor: Colors.white,
              child: Icon(
                Icons.add,
                color: primaryLightColor,
              ),
            ),
          ),
          body: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// this code for container image user and name and welcome screen
                  // Container(
                  //   height: 255,
                  //   decoration: BoxDecoration(
                  //       color: primaryColorNew,
                  //       borderRadius: BorderRadius.only(
                  //         bottomRight: Radius.circular(20),
                  //         bottomLeft: Radius.circular(20),
                  //       )),
                  //   child: Column(
                  //     children: [
                  //       Container(
                  //         alignment: Alignment.topRight,
                  //         decoration: BoxDecoration(
                  //             color: primaryColorNew,
                  //             borderRadius: BorderRadius.only(
                  //               bottomRight: Radius.circular(20),
                  //               bottomLeft: Radius.circular(20),
                  //             )),
                  //         child: getAccountData() == null
                  //             ? CircularProgressIndicator()
                  //             : Column(
                  //                 mainAxisAlignment: MainAxisAlignment.start,
                  //                 crossAxisAlignment: CrossAxisAlignment.start,
                  //                 children: [
                  //                   SizedBox(
                  //                     height: 60,
                  //                   ),
                  //                   Container(
                  //                     padding: EdgeInsets.only(right: 30),
                  //                     child: CircleAvatar(
                  //                         radius: 30,
                  //                         backgroundColor: Colors.white,
                  //                         backgroundImage: NetworkImage(
                  //                             userAccountJsonMap?['image'] ??
                  //                                 '')),
                  //                   ),
                  //                   SizedBox(
                  //                     height: 20,
                  //                   ),
                  //                   Padding(
                  //                       padding:
                  //                           const EdgeInsets.only(right: 35),
                  //                       child: CustomText(
                  //                         text:
                  //                             "اهلا ${_getOptimizedUserName()}",
                  //                         fontSize: 18.0,
                  //                         fontWeight: FontWeight.w500,
                  //                       )),
                  //                   SizedBox(
                  //                     height: 15,
                  //                   ),
                  //                   Padding(
                  //                       padding:
                  //                           const EdgeInsets.only(right: 35),
                  //                       child: CustomText(
                  //                         text: "مرحبا بك في كلينكو ",
                  //                         fontSize: 20.0,
                  //                         fontWeight: FontWeight.bold,
                  //                       )),
                  //                 ],
                  //               ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  SizedBox(
                    height: 55,
                  ),
                  AdsContainerWidget(ads: ads),
                  SizedBox(
                    height: 25,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 25.0),
                    child: CustomText(
                      text: " الاقسام",
                      fontSize: 22,
                      color: textColorblack,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  CategoresCardHome(homeCards: homeCards),
                  const SizedBox(
                    height: 25,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 25.0, left: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomText(
                          text: " المنتجات ",
                          fontSize: 22,
                          color: textColorblack,
                          fontWeight: FontWeight.w900,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      DevicesCategoriesFragment(
                                    isAdmin: false,
                                  ),
                                ));
                          },
                          child: CustomText(
                            text: "الكل  ",
                            fontSize: 25,
                            color: primaryColorNew,
                            decoration: TextDecoration.underline,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  BlocConsumer<GetProductCubit, GetProductStates>(
                    listener: (context, state) {
                      // TODO: implement listener
                    },
                    builder: (context, state) {
                      return ConditionalBuilder(
                        condition: getDataCategour.devices.length > 0,
                        builder: (BuildContext context) => Center(
                          child: Container(
                            padding: EdgeInsets.only(right: 20, left: 10),
                            height: 250,
                            child: ListView.separated(
                              itemCount: getDataCategour.devices.length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  onTap: () {

                                    print("herererr${getDataCategour.devices[index]}");
                                    // Navigator.of(context).push(MaterialPageRoute(
                                    //     builder: (BuildContext context) {
                                    //       return CategoryListScreen(
                                    //           getDataCategour.Categorus[index],
                                    //       );
                                    //     }));
                                  },
                                  child: Container(
                                    padding: EdgeInsets.only(top: 15),
                                    height: 230,
                                    width: 220,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    child: Center(
                                      child: Column(
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(20.0),
                                            child: Image.network(
                                              getDataCategour
                                                  .devices[index].images?.first,
                                              fit: BoxFit.fill,
                                              width: 190,
                                              height: 190,
                                            ),
                                          ),
                                          SizedBox(height: 15),
                                          Container(
                                            width: 60,
                                            child: CustomText(
                                              textAlign: TextAlign.center,
                                              text: getDataCategour
                                                  .devices[index].name
                                                  .toString(),
                                              fontSize: 15,
                                              maxLines: 2,
                                              color: textColorblack,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) => SizedBox(
                                width: 15,
                              ),
                            ),
                          ),
                        ),
                        fallback: (context) =>
                            Center(child: CircularProgressIndicator()),
                      );
                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 25.0, left: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomText(
                          text: " اقسام الاجهزة  ",
                          fontSize: 22,
                          color: textColorblack,
                          fontWeight: FontWeight.w900,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      DevicesCategoriesFragment(
                                    isAdmin: false,
                                  ),
                                ));
                          },
                          child: CustomText(
                            text: "الكل  ",
                            fontSize: 25,
                            color: primaryColorNew,
                            decoration: TextDecoration.underline,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  CategorisProdactes(
                    homeCards: homeCards,
                  ),
                ],
              )),
        ));
  }

  /// this code for container image user and name and welcome screen
// String _getOptimizedUserName() {
//   String? userName = userAccountJsonMap?['name'];
//   if (userName == null) {
//     return '';
//   } else {
//     List<String> splittedNameList = userName.split(' ');
//     return splittedNameList.length > 1
//         ? '${splittedNameList.first} ${splittedNameList.last}'
//         : splittedNameList.first;
//   }
// }
}
