import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class WidgetShimmer extends StatelessWidget {
  const WidgetShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Positioned(
        bottom: 32.0,
        left: 0.0,
        right: 0.0,
        child: Center(
          child: Opacity(
            opacity: 0.8,
            child: Shimmer.fromColors(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  // Image.asset(
                  //   'assets/images/chevron_right.png',
                  //   height: 20.0,
                  // ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4.0),
                  ),
                  const Text(
                    'Slide to unlock',
                    style: TextStyle(
                      fontSize: 28.0,
                    ),
                  )
                ],
              ),
              baseColor: Colors.black12,
              highlightColor: Colors.white,
            ),
          ),
        ));
  }
}
