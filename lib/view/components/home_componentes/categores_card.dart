import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../constants/app_colors.dart';
import '../../../model/home_card.dart';
import '../../all_clinics_map.dart';
import '../../get_clinic_appointment.dart';
import '../../screens/bmi_calculator/screens/bmi_input_page.dart';
import '../../screens/call_doctor.dart';
import '../../screens/clinics.dart';
import '../../screens/home_screen_App/devices_categories_fragment.dart';
import '../../screens/lab_rays_shared/labs/labs_screen.dart';
import '../../screens/lab_rays_shared/rays/rays_screen.dart';
import '../custom_text/custom_text.dart';

class CategoresCardHome extends StatelessWidget {
  const CategoresCardHome({
    Key? key,
    required this.homeCards,
  }) : super(key: key);

  final List<HomeCard> homeCards;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 23, left: 10),
      height: 160,
      child: ListView.separated(
        itemCount: homeCards.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              if (index == 0) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const CallDoctorScreen()));
              }
              if (index == 1) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                        const GetClinicAppointment()));
              }
              // if (index == 2)
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (context) => RaysScreen()));
              // if (index == 3)
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (context) => LabsScreen()));
              if (index == 2) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DevicesCategoriesFragment(
                        isAdmin: false,
                      ),
                    ));
              }
              if (index == 3) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const BmiInputPage()));
              }
              if (index == 4) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const Clinics()));
              }
              if (index == 5) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AllClinicsMap(
                          isPatient: true,
                        )));
              }
            },
            child: Container(
              padding: EdgeInsets.only(top: 15),
              height: 130,
              width: 100,
              decoration: BoxDecoration(

                  borderRadius: BorderRadius.circular(50),
                  color: Colors.white
              ),
              child: Column(
                children: [
                  Container(
                    child: Image.asset(
                      homeCards[index].pageImage!,
                      width: 80,
                      height: 80,
                    ),
                  ),
                  SizedBox(height: 15),
                  CustomText(
                    text: homeCards[index].pageName!,
                    fontSize: 18,
                    color:textColorblack,
                    fontWeight: FontWeight.w400,
                  )
                ],
              ),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) =>
            SizedBox(
              width: 15,
            ),
      ),
    );
  }
}
