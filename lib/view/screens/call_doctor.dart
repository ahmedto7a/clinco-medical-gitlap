import 'package:clinico/constants/app_colors.dart';
import 'package:clinico/view/screens/view_clinic_profile_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../model/clinic.dart';
import '../../model/user_data.dart';

class CallDoctorScreen extends StatefulWidget {
  // final String specialityId;
  // final int revealWayIndex;
  const CallDoctorScreen({
    Key? key,
  }) : super(key: key);

  @override
  _CallDoctorScreenState createState() => _CallDoctorScreenState();
}

class _CallDoctorScreenState extends State<CallDoctorScreen> {
  bool _isAdmin = false;
  List<Color> primaryGradientColors = AppColors.primaryGradientColors;
  List<Clinic> clinics = <Clinic>[];
  FirebaseFirestore? _firebaseFirestore;
  late CollectionReference clinicsRef;

  @override
  void initState() {
    super.initState();
    _firebaseFirestore = FirebaseFirestore.instance;
    clinicsRef =
        _firebaseFirestore!.collection(FirestoreCollections.Clinics.name);
  }

  @override
  Widget build(BuildContext context) {
    if (clinicsRef != null) {
      return Directionality(
          textDirection: TextDirection.rtl,
          child: Scaffold(
            appBar: AppBar(
              title: const Text(
                "مكالمة دكتور",
                style: TextStyle(fontSize: 20),
              ),
            ),
            body: Column(
              children: [
                Flexible(
                  child: clinicList(),
                ),
              ],
            ),
          ));
    } else {
      return const Center(child: CircularProgressIndicator());
    }
  }

  Widget clinicList() => StreamBuilder(
      stream: clinicsRef
          .where('revealWay', whereIn: [0, 1])
          .where('accountStatus', isNotEqualTo: 2)
          .orderBy('accountStatus', descending: true)
          .snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasData) {
          List<QueryDocumentSnapshot> docs = snapshot.data!.docs;
          if (docs.isEmpty) {
            return const Center(
                child: Text('عفواً، لا يوجد بيانات!',
                    style: TextStyle(color: Colors.blue)));
          }
          Map map = (docs).asMap();
          clinics.clear();
          map.forEach((dynamic, json) {
            clinics.add(Clinic.fromJson(json.data()));
          });
          return Directionality(
              textDirection: TextDirection.rtl,
              child: ListView.builder(
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, i) {
                    return Directionality(
                        textDirection: TextDirection.rtl,
                        child: GestureDetector(
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => ViewClinicProfileScreen(
                                        clinic: clinics[i],
                                        clinicId: docs[i].reference.id,
                                      ))),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Card(
                              // clipBehavior: Clip.antiAlias,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                decoration: BoxDecoration(
                                  // color: AppColors.appPrimaryColor
                                  //   border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(10),
                                  color:colorCard
                                ),
                                // decoration: BoxDecoration(gradient: LinearGradient(colors: primaryGradientColors, begin: Alignment.centerLeft, end: Alignment.centerRight,),),
                                height: 250,
                                padding:
                                    const EdgeInsets.only(top: 20, right: 18),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: 80,
                                            height: 80,
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                image: DecorationImage(
                                                    image: NetworkImage(
                                                        clinics[i].logo ?? ''),
                                                    fit: BoxFit.fill)),
                                          ),
                                          SizedBox(width: 25,),
                                          Container(
                                            width: 270,
                                            padding: const EdgeInsets.only(
                                                top: 5, bottom: 5),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                      clinics[i].name ?? '',
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      maxLines: 1,
                                                      textDirection:
                                                          TextDirection.rtl,
                                                      style: const TextStyle(
                                                          fontSize: 18.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: backgroundIcon)),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Container(
                                                  child: Text(
                                                      clinics[i].about ?? '',
                                                      overflow:
                                                      TextOverflow.ellipsis,
                                                      maxLines: 1,
                                                      textDirection:
                                                      TextDirection.rtl,
                                                      style: const TextStyle(
                                                          fontSize: 15.0,
                                                          fontWeight:
                                                          FontWeight.bold,
                                                          color: textColorblack
                                                      )),
                                                ),

                                              ],
                                            ),
                                          ),
                                        ]),
                                    SizedBox(
                                      height: 35,
                                    ),
                                    // this address
                                    Container(
                                      alignment: Alignment.topRight,
                                      child:const Text(
                                      " خليك في البيت و اكشف اونلاين او من العيادة ",
                                          overflow:
                                          TextOverflow.ellipsis,
                                          maxLines: 1,
                                          textDirection:
                                          TextDirection.rtl,
                                          style: const TextStyle(
                                              fontSize: 15.0,
                                              fontWeight:
                                              FontWeight.bold,
                                              color: textColorblack)),
                                    ),
                                    SizedBox(height: 15.5,),
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(Icons.location_on_outlined,color: backgroundIcon,size: 30,),
                                        SizedBox(width: 5.5,),
                                        Column(
                                          children: [
                                            Text("العنوان",
                                                overflow:
                                                TextOverflow.ellipsis,
                                                maxLines: 1,
                                                textDirection:
                                                TextDirection.rtl,
                                                style: const TextStyle(
                                                    fontSize: 15.0,
                                                    fontWeight:
                                                    FontWeight.bold,
                                                    color: Colors.grey)),
                                            Text(
                                                clinics[i].address ?? '',
                                                overflow:
                                                TextOverflow.ellipsis,
                                                maxLines: 1,
                                                textDirection:
                                                TextDirection.rtl,
                                                style: const TextStyle(
                                                    fontSize: 15.0,
                                                    fontWeight:
                                                    FontWeight.bold,
                                                    color: textColorblack)),
                                          ],
                                        ),
                                        SizedBox(width: 60,),
                                       const Text("\$ ",
                                            overflow:
                                            TextOverflow.ellipsis,
                                            maxLines: 1,
                                            textDirection:
                                            TextDirection.rtl,
                                            style:  TextStyle(
                                                fontSize: 37.0,
                                                fontWeight:
                                                FontWeight.w600,
                                                color:backgroundIcon)),
                                        SizedBox(width: 5.5,),
                                        Column(
                                          children: [
                                            const Text(" سعر الكشف ",
                                                overflow:
                                                TextOverflow.ellipsis,
                                                maxLines: 1,
                                                textDirection:
                                                TextDirection.rtl,
                                                style:  TextStyle(
                                                    fontSize: 15.0,
                                                    fontWeight:
                                                    FontWeight.bold,
                                                    color: Colors.grey)),
                                            SizedBox(height: 0.5,),
                                            Text(
                                                "${clinics[i].price.toString()} جنيها " ?? '',
                                                overflow:
                                                TextOverflow.ellipsis,
                                                maxLines: 1,
                                                textDirection:
                                                TextDirection.rtl,
                                                style: const TextStyle(
                                                    fontSize: 15.0,
                                                    fontWeight:
                                                    FontWeight.bold,
                                                    color: textColorblack)),
                                          ],
                                        ),

                                      ],
                                    ),
                                    _isAdmin == true
                                        ? Flexible(
                                        flex: 3,
                                        fit: FlexFit.loose,
                                        child: Text(
                                            clinics[i].workIsAvailable !=
                                                true
                                                ? ' غير متوفر الان '
                                                : " متاح التدريب ",
                                            overflow: TextOverflow
                                                .ellipsis,
                                            maxLines: 1,
                                            textDirection:
                                            TextDirection.rtl,
                                            style: const TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.bold,
                                                color: textColorblack)))
                                        : Flexible(
                                      flex: 0,
                                      fit: FlexFit.loose,
                                      child: Text(""),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ));
                  }));
        } else if (snapshot.hasError) {
          return const Center(
              child: Text(
            'عفواً، حدث خطأ ما!',
            style: TextStyle(color: Colors.red),
          ));
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        }
        return const Center(
            child: Text(
          'عفواً، حدث خطأ ما!',
          style: TextStyle(color: Colors.red),
        ));
      });
}
