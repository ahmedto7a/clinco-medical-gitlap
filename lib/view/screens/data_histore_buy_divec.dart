import 'package:clinico/constants/app_colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../model/bought_devices.dart';


// todo for this cart pages
class DataHistoreBuyDivec extends StatefulWidget {
  const DataHistoreBuyDivec({Key? key}) : super(key: key);

  @override
  State<DataHistoreBuyDivec> createState() => _DataHistoreBuyDivecState();
}

class _DataHistoreBuyDivecState extends State<DataHistoreBuyDivec> {

  bool pageIsLoading = false;
  final DateFormat timeFormat =
          DateFormat('EEEE  yyyy/MM/dd - hh:mm a', 'ar_KSA'),
      dayFormatEnglish = DateFormat('EEE');
  String? _valueText = '';
  List<DevicesHistory> devicesHistory = [];
  @override



  var boughtData = FirebaseFirestore.instance
      .collection("DevicesHistory")
      .where("buyer", isEqualTo: FirebaseAuth.instance.currentUser!.uid)
      .orderBy("date", descending: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 15,
        ),
        body: StreamBuilder(
          stream: boughtData.snapshots(),
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              final docs = snapshot.data!.docs;
              if (docs.isEmpty) {
                return const Center(
                  child: Text("لا يوجد عمليات شراء"),
                );
              } else {
                devicesHistory.clear();
                docs.forEach((element) {

                  if( !devicesHistory.contains(devicesHistory) ){

                    devicesHistory.add(DevicesHistory.fromJson(element.data()));
                   print(devicesHistory);
                  }
                });
                return Column(
                  children: [
                    Expanded(
                      child: ListView.builder(
                        itemCount: devicesHistory.length,
                        itemBuilder: (ctx, index) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.grey.withOpacity(0.15)),
                              height: 160,
                              padding: const EdgeInsets.all(4),
                              child: Row(
                                children: [
                                  if (devicesHistory[index].images!.isNotEmpty)
                                    SizedBox(
                                      width: 140,
                                      height: 140,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.network(
                                          devicesHistory[index].images!.first,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                      child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 15),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(devicesHistory[index].name!,
                                            style: const TextStyle(
                                                fontSize: 18,
                                                color: textColorblack,
                                                fontWeight: FontWeight.bold)),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(devicesHistory[index].manufacture!,
                                            style: const TextStyle(
                                                fontSize: 18,
                                                color: textColor,
                                                fontWeight: FontWeight.bold)),
                                        const Spacer(),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                               " ${TotallPrice(devicesHistory)}",
                                                style: const TextStyle(
                                                    fontSize: 20,
                                                    color: textColorblack,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Colors.grey
                                                      .withOpacity(0.15)),
                                              width: 110,
                                              height: 30,
                                              child: Row(
                                                children: [
                                                  Text(
                                                      "القمية المطلوبة : ",
                                                      style: const TextStyle(
                                                          fontSize: 15,
                                                          color: textColorblack,
                                                          fontWeight:
                                                          FontWeight.bold)),
                                                  const Spacer(),
                                                  Text(
                                                      "${devicesHistory[index]!.quantity} ",
                                                      style: const TextStyle(
                                                          fontSize: 20,
                                                          color: textColorblack,
                                                          fontWeight:
                                                              FontWeight.bold)),
                                                  const Spacer(),

                                                ],
                                              ),
                                            )
                                          ],
                                        ),

                                        // Text(
                                        //   timeFormat.format(
                                        //       devicesHistory[index].date!.toDate()),
                                        //   style: const TextStyle(
                                        //       fontSize: 15,
                                        //       color: Colors.white,
                                        //       fontWeight: FontWeight.bold),
                                        // ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                      ],
                                    ),
                                  )),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 20),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 10,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(" اجمالي الطلب ",
                                  style: const TextStyle(
                                      fontSize: 18,
                                      color: textColor,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 5,
                              ),
                              Text(" 120 جنية ",
                                  style: const TextStyle(
                                      fontSize: 18,
                                      color: backgroundIcon,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          Spacer(),
                          SizedBox(
                            width: 150,
                            height: 50,
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  primary: primaryColorNew, // background
                                  // foreground
                                ),
                                onPressed: () {},
                                child: const Center(
                                  child: Text(" اتمام الطلب  ",
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: textColorWiht,
                                          fontWeight: FontWeight.w600)),
                                )),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    )
                  ],
                );
              }
            }
            if (snapshot.hasError) {
              return const Center(
                  child: Text(
                'عفواً، حدث خطأ ما!',
                style: TextStyle(color: Colors.red),
              ));
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }
            return const Center(
                child: Text(
              'عفواً، حدث خطأ ما!',
              style: TextStyle(color: Colors.red),
            ));
          },
        ));
  }

  TotallPrice(List<DevicesHistory> devicesHistory) {
    var price = 0;
    for (var allProduct in devicesHistory){
      // price = allProduct.quantity * allProduct.price;
    }

  }
}
