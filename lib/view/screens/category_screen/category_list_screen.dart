import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants/app_colors.dart';
import '../../../model/ad.dart';
import '../../../model/devices_category.dart';
import '../../../view_model/cubuit/get_categore_from_firebase_cubuit/cubuit.dart';
import '../../../view_model/cubuit/get_categore_from_firebase_cubuit/states.dart';
import '../../../view_model/cubuit/get_producat/cubuit.dart';
import '../../../view_model/cubuit/get_producat/states.dart';
import '../../components/custom_text/custom_text.dart';
import '../../components/home_componentes/ads_container_widget.dart';

class CategoryListScreen extends StatelessWidget {
  DevicesCategory modelDataCategoryListScreen;

  CategoryListScreen(this.modelDataCategoryListScreen);

  @override
  Widget build(BuildContext context) {
    var getDataProduct = BlocProvider.of<GetProductCubit>(
        context, listen: true);
    return Scaffold(
      appBar: AppBar(
        title: Text(modelDataCategoryListScreen.arabicTitle.toString()),
        actions: [
          IconButton(onPressed: () {}, icon: Icon(Icons.search))
        ],
      ),
      body:  BlocConsumer<GetProductCubit, GetProductStates>(
        listener: (context, state) {
          // TODO: implement listener
        },
        builder: (context, state) {
          return ConditionalBuilder(
            condition: getDataProduct.devices.length > 0,
            builder: (BuildContext context) =>
                Center(
                  child: Container(
                    padding: EdgeInsets.only(right: 23, left: 10),
                    height: 160,
                    child: ListView.separated(
                      itemCount: getDataProduct.devices.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            // Navigator.of(context).push(MaterialPageRoute(
                            //     builder: (BuildContext context) {
                            //       return CategoryListScreen(
                            //         getDataProduct.devices[index].name.toString(),
                            //       );
                            //     }));
                          },
                          child: Container(
                            padding: EdgeInsets.only(top: 15),
                            height: 130,
                            width: 120,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: Center(
                              child: Column(
                                children: [
                                  // ClipRRect(
                                  //   borderRadius: BorderRadius.circular(40.0),
                                  //   child: Image.network(
                                  //     getDataProduct.devices[index].images.toString(),
                                  //     fit: BoxFit.fill,
                                  //     width: 90,
                                  //     height: 90,
                                  //   ),
                                  // ),
                                  SizedBox(height: 15),
                                  Container(
                                    width: 60,
                                    child: CustomText(
                                      textAlign: TextAlign.center,
                                      text: getDataProduct.devices[index]
                                          .name.toString(),
                                      fontSize: 15,
                                      maxLines: 2,
                                      color: textColorblack,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          SizedBox(
                            width: 15,
                          ),
                    ),
                  ),
                ),
            fallback: (context) =>  Center(
                child: CircularProgressIndicator()
            ),
          );
        },
      )
    );
  }
}
