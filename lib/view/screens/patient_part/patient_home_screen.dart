import 'package:badges/badges.dart';
import 'package:clinico/view/components/custom_text/custom_text.dart';
import 'package:clinico/view/screens/patient_part/fragments/patient_clinics_appointments_fragment.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_icons/line_icons.dart';
import '../../../constants/app_colors.dart';
import '../../../view_model/cubuit/get_diveces_history/cubuit.dart';
import '../../../view_model/cubuit/get_diveces_history/states.dart';
import '../data_histore_buy_divec.dart';
import '../home_screen_App/home_screen_app.dart';
import '../home_screen_App/more_fragment.dart';
class PatientHomeScreen extends StatefulWidget {
  const PatientHomeScreen({Key? key}) : super(key: key);

  @override
  _PatientHomeScreenState createState() => _PatientHomeScreenState();
}

class _PatientHomeScreenState extends State<PatientHomeScreen> {
  List<Color> primaryGradientColors = AppColors.primaryGradientColors;
  Color primaryLightColor = AppColors.primaryColorold;
  DateTime? currentBackPressTime;
  int selectedIndex = 0;
  final widgetTitle = [
    'العيادات',
    'الحجوزات',
    'الأجهزة',
    'الصفحة الشخصية',
    'المزيد'
  ];
  final widgetOptions = [
    const HomeScreenApp(),
    const PatientClinicsAppointmentsFragment(
      screenTitle: 'حجوزات العيادات',
    ),
    const DataHistoreBuyDivec(),
    // DevicesCategoriesFragment(isAdmin: false,),
    // const BmiInputPage(),
    const MoreFragment()
  ];

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;

    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Directionality(
          textDirection: TextDirection.rtl, child: getScreenScaffoldWidget()),
    );
  }

  Scaffold getScreenScaffoldWidget() {
    return Scaffold(
        body: IndexedStack(
          index: selectedIndex,
          children: widgetOptions,
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(LineIcons.home), label: 'الرئيسية'),
            BottomNavigationBarItem(
                icon: Icon(LineIcons.calendarCheckAlt), label: 'مواعيدى'),
            BottomNavigationBarItem(
                icon: BlocConsumer<GetDeviceHistory, GetDevicesHistoryStates>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    var getDeviceHistory = BlocProvider.of<GetDeviceHistory>(context, listen: true);
                    return Badge(
                      badgeColor: primaryColorNew,
                      shape: BadgeShape.circle,
                      animationType: BadgeAnimationType.slide,
                      position: BadgePosition.topStart(),
                      borderRadius: BorderRadius.circular(100),
                      child: Icon(Icons.shopping_cart_outlined),
                      badgeContent: Padding(
                        padding: const EdgeInsets.only(top: 2.0),
                        child: CustomText(
                          text: getDeviceHistory.divesHistory.length.toString(),
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      showBadge: getDeviceHistory.divesHistory.length > 0 ? true : false,
                    );
                  },
                ),
                label: 'عربة التسوق'),
            BottomNavigationBarItem(icon: Icon(LineIcons.user), label: 'حسابى'),
          ],
          currentIndex: selectedIndex,
          onTap: onItemTapped,
          elevation: 0,
          selectedItemColor: primaryColorNew,
          selectedLabelStyle: TextStyle(color: primaryColorNew),
          unselectedLabelStyle: const TextStyle(color: Colors.grey),
          showSelectedLabels: true,
          showUnselectedLabels: true,
          selectedIconTheme: IconThemeData(color: primaryColorNew),
          unselectedItemColor: Colors.grey,
          // type: BottomNavigationBarType.fixed,
          // selectedLabelStyle: TextStyle(color: primaryLightColor, fontSize: 10),
          // selectedIconTheme: const IconThemeData(color: Color(0xFFF80144), opacity: 1.0, size: 30.0),
          // selectedItemColor: const Color(0xFFF80144),
          // showSelectedLabels: true,
          // unselectedIconTheme: const IconThemeData(color: AppColors.primaryColor, opacity: 1.0, size: 30.0),
          // unselectedFontSize: 16,
          // unselectedItemColor: AppColors.primaryColor,
          // unselectedLabelStyle: const TextStyle(fontSize: 8, color: AppColors.primaryColor),
          // showUnselectedLabels: false,
          // backgroundColor: Colors.white,
        ));
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime!) > const Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Click again to exit the application!");
      return Future.value(false);
    }
    return Future.value(true);
  }
}
