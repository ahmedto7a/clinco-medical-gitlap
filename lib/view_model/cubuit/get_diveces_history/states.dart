import 'package:flutter/material.dart';

abstract class GetDevicesHistoryStates {}
class AuthInitializedStates extends GetDevicesHistoryStates {}

class GetDataDevicesHistoryLoding extends GetDevicesHistoryStates{}
class GetDataDevicesHistoryStatesSuccess extends GetDevicesHistoryStates{}
class GetDataDevicesHistoryErrorStates extends GetDevicesHistoryStates{
  final String error;
  GetDataDevicesHistoryErrorStates(this.error);
}
